//
//  Stickyness.c
//  Puzzle
//
//  Created by Bogus on 24.06.2013.
//
//

#include "Stickyness.h"
#include "Memory.h"
#include <math.h>
#include "chipmunk_unsafe.h"
#include "Vertices.h"
#include "Globals.h"

#define DIST_THRESHOLD 10.0f

static cpBool arbiterBodiesTouched(cpArbiter *arb)
{
//    CP_ARBITER_GET_BODIES(arb, a, b);
//    ElementSprite *spriteA = cpBodyGetUserData(a);
//    ElementSprite *spriteB = cpBodyGetUserData(b);
//    return spriteA.touched && spriteB.touched;
    
    return true;
}

cpBool stickyBegin(cpArbiter *arb, cpSpace *space, void *data)
{
    if (!arbiterBodiesTouched(arb)) {
        return false;
    }
    
    CP_ARBITER_GET_BODIES(arb, a, b)
    uint *numACollisions = cpBodyGetUserData(a);
    uint *numBCollisions = cpBodyGetUserData(b);
    (*numACollisions)++;
    (*numBCollisions)++;
    
    //    cpVect *f = malloc(sizeof(cpVect));
    //    *f = cpvzero;
    //    cpArbiterSetUserData(arb, f);
    
    return true;
}

static void addJointPostStep(cpSpace *space, void *key, void *data)
{
    cpConstraint *joint = (cpConstraint *)data;
    cpSpaceAddConstraint(space, joint);
}

static void moveShape(cpBody *b, cpShape *shape, void *data)
{
    cpBody *a = (cpBody *)data;
    cpSpace *space = cpBodyGetSpace(b);
    cpSpaceRemoveShape(space, shape);
    
    // modify shape - unsafe chipmunk API!!!
    POLY_SHAPE_GET_VERTS(shape, num, verts)
    cpVect offset = cpvsub(b->p, a->p);
    cpPolyShapeSetVerts(shape, num, verts, offset);
    
    cpShapeSetBody(shape, a);
    cpSpaceAddShape(space, shape);
}

void removeConstraint(cpBody *body, cpConstraint *constraint, void *unused)
{
    constraintFreeWrap(cpBodyGetSpace(body), constraint, unused);
}

static void combineBodiesPostStep(cpSpace *space, void *key, void *data)
{
    cpArbiter *arb = (cpArbiter *)data;
    CP_ARBITER_GET_BODIES(arb, a, b)
    if (a == b) {
        return;
    }
    
    cpBodyEachShape(b, moveShape, a);
    cpBodyEachConstraint(b, removeConstraint, NULL);
    cpSpaceRemoveBody(space, b);
    cpBodyFree(b);
}

cpVect calculateForce(cpShape *aShp, cpShape *bShp, cpBool *shouldStop)
{
    if (shouldStop != NULL) {
        *shouldStop = false;
    }
    cpBody *a = cpShapeGetBody(aShp);
    cpBody *b = cpShapeGetBody(bShp);
    
    // get sensor shapes vertices
    POLY_SHAPE_GET_VERTS(aShp, aNum, aVerts)
    POLY_SHAPE_GET_VERTS(bShp, bNum, bVerts)
    
    // get core shapes vertices
    POLY_SHAPE_GET_VERTS(cpShapeGetUserData(aShp), aPNum, aPVerts)
    POLY_SHAPE_GET_VERTS(cpShapeGetUserData(bShp), bPNum, bPVerts)
    vertsLocal2World(a, aPVerts, aPNum, aPVerts);
    vertsLocal2World(b, bPVerts, bPNum, bPVerts);
    
    // convert local vertices to global
    cpVect aWorldVerts[aNum];
    cpVect bWorldVerts[bNum];
    vertsLocal2World(a, aVerts, aNum, aWorldVerts);
    vertsLocal2World(b, bVerts, bNum, bWorldVerts);
    
    // get collision area vertices, centroid and area
    INTERSECT_VERTS_WITH_VERTS(aWorldVerts, aNum, bWorldVerts, bNum, collVerts, collNum)
    if (collNum == 0) { // no collision - why??
        return cpvzero;
    }
    cpVect collCenter = cpCentroidForPoly(collNum, collVerts);
    cpFloat collArea = cpAreaForPoly(collNum, collVerts);
    if (collArea == 0) {
        return cpvzero;
    }
    
    // calculate nearest edge centers
    cpVect aEdgeCenterProj;
    cpVect bEdgeCenterProj;
    cpVect aEdgeCenter = vertsNearestEdgeCenter(aPVerts, aPNum, collCenter, &aEdgeCenterProj);
    vertsNearestEdgeCenter(bPVerts, bPNum, collCenter, &bEdgeCenterProj);
    
    // calculate distance between edges
    cpVect edgeSub = cpvmult(cpvsub(bEdgeCenterProj, aEdgeCenterProj), 0.5);
    // stop condition
    if (shouldStop != NULL) {
        *shouldStop = cpvlength(edgeSub) < 1.0f;
        if (*shouldStop) {
            // correct body position and d vector
            cpBodySetPos(a, cpvadd(cpBodyGetPos(a), edgeSub));
            cpBodySetPos(b, cpvadd(cpBodyGetPos(b), cpvneg(edgeSub)));
            
            return cpvmult(cpvnormalize(cpvsub(aEdgeCenter, aEdgeCenterProj)), collArea);
        }
    }
    
    // calculate new force
    return cpvmult(cpvnormalize(cpvsub(aEdgeCenter, collCenter)), collArea);
}

static void trimShapes(cpBody *body, cpShape *shape, void *data)
{
    if (cpShapeGetCollisionType(shape) != COLLIDE_STICK_SENSOR) {
        return;
    }
    cpShape *aShp = shape;
    cpShape *bShp = (cpShape *)data;
    
    // get sensor shapes vertices
    POLY_SHAPE_GET_VERTS(aShp, aNum, aVerts)
    POLY_SHAPE_GET_VERTS(bShp, bNum, bVerts)
    
    cpBool shouldTrim;
    cpVect F = calculateForce(aShp, bShp, &shouldTrim);
    
    if (shouldTrim) {
        cpVect clipVect = cpvmult(cpvnormalize(F), STICKY_PADDING);
        
        CLIP_VERTS_WITH_VECT(aVerts, aNum, clipVect, clipAVerts, clipANum)
        cpPolyShapeSetVerts(aShp, clipANum, clipAVerts, cpvzero); // unsafe?
        
        CLIP_VERTS_WITH_VECT(bVerts, bNum, cpvneg(clipVect), clipBVerts, clipBNum)
        cpPolyShapeSetVerts(bShp, clipBNum, clipBVerts, cpvzero); // unsafe?
    }
}

cpBool stickyPreSolve(cpArbiter *arb, cpSpace *space, void *data)
{
    CP_ARBITER_GET_BODIES(arb, a, b)
    CP_ARBITER_GET_SHAPES(arb, aShp, bShp)
    
    cpBool shouldStop;
    cpVect F = calculateForce(aShp, bShp, &shouldStop);
    
    if (shouldStop) {
        // trim all collision shapes
        cpBodyEachShape(a, (cpBodyShapeIteratorFunc)trimShapes, bShp);
        cpBodyEachShape(b, (cpBodyShapeIteratorFunc)trimShapes, aShp);
        
        // reset forces and velocities
        cpBodyResetForces(a);
        cpBodyResetForces(b);
        cpBodySetVel(a, cpvzero);
        cpBodySetVel(b, cpvzero);
        
        // combine bodies callback
        cpSpaceAddPostStepCallback(space, combineBodiesPostStep, a, arb);
    } else {
        // update forces
        cpBodySetForce(a, cpvneg(F));
        cpBodySetForce(b, F);
    }
    
    // don't use postSolve callback
    return false;
}

void stickyPostSolve(cpArbiter *arb, cpSpace *space, void *data)
{
    
}

static void removeJointPostStep(cpSpace *space, void *key, void *data)
{
    cpConstraint *joint = (cpConstraint *)key;
    cpSpaceRemoveConstraint(space, joint);
    cpConstraintFree(joint);
}

void stickySeparate(cpArbiter *arb, cpSpace *space, void *data)
{
    if (arbiterBodiesTouched(arb))
    {
        
        cpConstraint *joint = cpArbiterGetUserData(arb);
        if (joint) {
            cpConstraintSetMaxForce(joint, 0);
            cpSpaceAddPostStepCallback(space, removeJointPostStep, joint, NULL);
            cpArbiterSetUserData(arb, NULL);
        }
        
        //        cpVect *f = cpArbiterGetUserData(arb);
        //        free(f);
        //        cpArbiterSetUserData(arb, NULL);
    }
    
    
    
    CP_ARBITER_GET_BODIES(arb, a, b)
    
    uint *numACollisions = cpBodyGetUserData(a);
    uint *numBCollisions = cpBodyGetUserData(b);
    (*numACollisions)--;
    (*numBCollisions)--;
    
    a->f = cpvzero;
    b->f = cpvzero;
    a->v = cpvzero;
    b->v = cpvzero;
}
