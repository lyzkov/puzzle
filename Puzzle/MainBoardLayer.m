//
//  HelloWorldLayer.m
//  Puzzle
//
//  Created by Bogus on 07.02.2013.
//  Copyright __MyCompanyName__ 2013. All rights reserved.
//

#import "AppDelegate.h"

// Import the interfaces
#import "MainBoardLayer.h"
#import "Piece.h"
#import "CPDebugLayer.h"
#import "Memory.h"

enum {
	kTagParentNode = 1,
};

#pragma mark - HelloWorldLayer

@interface MainBoardLayer ()
- (void)addNewSpriteAtPosition:(CGPoint)pos;
- (void)createMenu;
- (void)initPhysics;
@end


@implementation MainBoardLayer

@synthesize space = space_;

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+ (CCScene *)scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	MainBoardLayer *layer = [[[MainBoardLayer alloc] initWithScene:scene] autorelease];
	
	// add layer as a child to scene
	[scene addChild:layer z:0];
	
	// return the scene
	return scene;
}

- (id)initWithScene:(CCScene *)scene
{
	if( (self=[super init])) {
        
        // set weak ref to scene
        scene_ = scene;
        
        // enable events
		self.isTouchEnabled = YES;
		self.isAccelerometerEnabled = NO;
		
		CGSize s = [[CCDirector sharedDirector] winSize];
		
		// title
		CCLabelTTF *label = [CCLabelTTF labelWithString:@"Multi touch the screen" fontName:@"Marker Felt" fontSize:36];
		label.position = ccp( s.width / 2, s.height - 30);
		[self addChild:label z:-1];
		
		// reset button
		[self createMenu];
		
		// init physics
		[self initPhysics];
		
		// Use batch node. Faster
		CCSpriteBatchNode *parent = [CCSpriteBatchNode batchNodeWithFile:@"Penelope_Cruz_16.jpg" capacity:100];
		spriteTexture_ = [parent texture];
		[self addChild:parent z:-1 tag:kTagParentNode];
		
		[self addNewSpriteAtPosition:ccp(200,200)];
		
		[self scheduleUpdate];
	}
	
	return self;
}

-(void) initPhysics
{
	CGSize s = [[CCDirector sharedDirector] winSize];
	
	space_ = cpSpaceNew();
	
	cpSpaceSetGravity( space_, cpv(0, 0) );
	
	//
	// rogue shapes
	// We have to free them manually
	//
	// bottom
	walls_[0] = cpSegmentShapeNew( space_->staticBody, cpv(0,0), cpv(s.width,0), 0.0f);
	
	// top
	walls_[1] = cpSegmentShapeNew( space_->staticBody, cpv(0,s.height), cpv(s.width,s.height), 0.0f);
	
	// left
	walls_[2] = cpSegmentShapeNew( space_->staticBody, cpv(0,0), cpv(0,s.height), 0.0f);
	
	// right
	walls_[3] = cpSegmentShapeNew( space_->staticBody, cpv(s.width,0), cpv(s.width,s.height), 0.0f);
	
	for( int i=0;i<4;i++) {
		cpShapeSetElasticity( walls_[i], 1.0f );
		cpShapeSetFriction( walls_[i], 1.0f );
		cpSpaceAddStaticShape(space_, walls_[i] );
	}
    
    // add debug layer
#if DEBUG == 1
    [scene_ addChild:[CPDebugLayer debugLayerForSpace:space_ options:nil] z:1];
#endif
}

- (void)dealloc
{
	// must remove these BEFORE freeing the body or you will access dangling pointers
	cpSpaceEachShape(space_, (cpSpaceShapeIteratorFunc)postShapeFree, space_);
	cpSpaceEachConstraint(space_, (cpSpaceConstraintIteratorFunc)postConstraintFree, space_);
    
	cpSpaceEachBody(space_, (cpSpaceBodyIteratorFunc)postBodyFree, space_);
	
	cpSpaceFree(space_);
	
	[super dealloc];
	
}

- (void)update:(ccTime)delta
{
	// Should use a fixed size step based on the animation interval.
	int steps = 2;
	CGFloat dt = [[CCDirector sharedDirector] animationInterval]/(CGFloat)steps;
	
	for(int i=0; i<steps; i++){
		cpSpaceStep(space_, dt);
	}
}

- (void)draw
{
    [super draw];
#if DEBUG == 1
	ccDrawColor4B(255,0,0,255);
    for (Piece *piece in [self getChildByTag:kTagParentNode].children) {
        CGPoint endpoint = ccpAdd(piece.position, ccp(piece.body->f.x, piece.body->f.y));
        ccDrawLine(piece.position, endpoint);
//        CCLOG(@"Sprite position: %.2f %.2f", sprite.position.x, sprite.position.y);
    }
#endif
}

- (void)createMenu
{
	// Default font size will be 22 points.
	[CCMenuItemFont setFontSize:22];
	
	// Reset Button
	CCMenuItemLabel *reset = [CCMenuItemFont itemWithString:@"Reset" block:^(id sender){
		[[CCDirector sharedDirector] replaceScene: [MainBoardLayer scene]];
	}];
	
	// Achievement Menu Item using blocks
	CCMenuItem *itemAchievement = [CCMenuItemFont itemWithString:@"Achievements" block:^(id sender) {
		
		
		GKAchievementViewController *achivementViewController = [[GKAchievementViewController alloc] init];
		achivementViewController.achievementDelegate = self;
		
		AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
		
		[[app navController] presentModalViewController:achivementViewController animated:YES];
		
		[achivementViewController release];
	}];
	
	// Leaderboard Menu Item using blocks
	CCMenuItem *itemLeaderboard = [CCMenuItemFont itemWithString:@"Leaderboard" block:^(id sender) {
		
		
		GKLeaderboardViewController *leaderboardViewController = [[GKLeaderboardViewController alloc] init];
		leaderboardViewController.leaderboardDelegate = self;
		
		AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
		
		[[app navController] presentModalViewController:leaderboardViewController animated:YES];
		
		[leaderboardViewController release];
	}];
	
	CCMenu *menu = [CCMenu menuWithItems:itemAchievement, itemLeaderboard, reset, nil];
	
	[menu alignItemsVertically];
	
	CGSize size = [[CCDirector sharedDirector] winSize];
	[menu setPosition:ccp( size.width/2, size.height/2)];
	
	
	[self addChild: menu z:-1];
}

-(void) addNewSpriteAtPosition:(CGPoint)pos
{
	int posx, posy;
    int numx, numy; // number of tiles
	
	CCNode *parent = [self getChildByTag:kTagParentNode];
    CGSize textureSize = spriteTexture_.contentSize;
    
#define TILE_WIDTH 100.0f
#define TILE_HEIGHT 100.0f
    
    numx = textureSize.width / TILE_WIDTH;
    numy = textureSize.height / TILE_HEIGHT;
	
	posx = CCRANDOM_0_1() * 200.0f;
	posy = CCRANDOM_0_1() * 200.0f;
	
	posx = (posx % numx) * TILE_WIDTH;
	posy = (posy % numy) * TILE_HEIGHT;
	
    Piece *piece = [Piece pieceWithTexture:spriteTexture_ rect:CGRectMake(posx, posy, TILE_WIDTH, TILE_HEIGHT) inBoard:self];
//	[parent addChild: sprite];
    [parent addChild:piece z:-1]; // z axis change to draw debug lines on sprites
	
	piece.position = pos;
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	for( UITouch *touch in touches ) {
		CGPoint location = [touch locationInView: [touch view]];
		location = [[CCDirector sharedDirector] convertToGL: location];
		
		[self addNewSpriteAtPosition: location];
	}
}

//- (void)accelerometer:(UIAccelerometer*)accelerometer didAccelerate:(UIAcceleration*)acceleration
//{
//	static float prevX=0, prevY=0;
//
//#define kFilterFactor 0.05f
//
//	float accelX = (float) acceleration.x * kFilterFactor + (1- kFilterFactor)*prevX;
//	float accelY = (float) acceleration.y * kFilterFactor + (1- kFilterFactor)*prevY;
//
//	prevX = accelX;
//	prevY = accelY;
//
//	cpVect v;
//	if( [[CCDirector sharedDirector] interfaceOrientation] == UIInterfaceOrientationLandscapeRight )
//		v = cpv( -accelY, accelX);
//	else
//		v = cpv( accelY, -accelX);
//
//	cpSpaceSetGravity( space_, cpvmult(v, 200) );
//}


#pragma mark GameKit delegate

-(void) achievementViewControllerDidFinish:(GKAchievementViewController *)viewController
{
	AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}

-(void) leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
	AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}

@end
