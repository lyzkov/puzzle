//
//  Memory.h
//  Puzzle
//
//  Created by Bogus on 25.06.2013.
//
//

#ifndef Puzzle_Memory_h
#define Puzzle_Memory_h

#include "chipmunk.h"

void shapeFreeWrap(cpSpace *space, cpShape *shape, void *unused);

void postShapeFree(cpShape *shape, cpSpace *space);

void constraintFreeWrap(cpSpace *space, cpConstraint *constraint, void *unused);

void postConstraintFree(cpConstraint *constraint, cpSpace *space);

void bodyFreeWrap(cpSpace *space, cpBody *body, void *unused);

void postBodyFree(cpBody *body, cpSpace *space);

#endif
