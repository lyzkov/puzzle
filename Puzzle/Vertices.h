//
//  Vertices.h
//  Puzzle
//
//  Created by Bogus on 12.07.2013.
//
//

#ifndef Puzzle_Vertices_h
#define Puzzle_Vertices_h

#include "chipmunk.h"

#define POLY_SHAPE_GET_VERTS(shape, num, verts) \
int num = cpPolyShapeGetNumVerts(shape); \
cpVect verts[num]; \
for (int i = 0; i < num; i++) { \
verts[i] = cpPolyShapeGetVert(shape, i); \
}

#define CLIP_VERTS_WITH_VECT(verts, num, vect, result, resultNum) \
cpVect result[num+1]; \
int resultNum = vertsClipWithVect(verts, num, vect, result, NULL);

#define CLIP_VERTS_WITH_VECT_COMPL(verts, num, vect, result, resultNum) \
cpVect result[num+1]; \
int resultNum = vertsCLipWithVect(verts, num, vect, NULL, result); \
resultNum = num - resultNum + 4;

#define INTERSECT_VERTS_WITH_VERTS(subVerts, subNum, clipVerts, clipNum, resultVerts, resultNum) \
cpVect resultVerts[clipNum*2]; \
int resultNum = vertsIntersectWithVerts(subVerts, subNum, clipVerts, clipNum, resultVerts);

cpVect edgeClipHorizontaly(cpVect A, cpVect B, float x);

uint vertsClipWithVect(const cpVect* verts, uint num, const cpVect clipVect, cpVect *resultVerts, cpVect *compVerts);

uint vertsIntersectWithVerts(const cpVect *subjectVerts, uint subjectNum, const cpVect *clipVerts, uint clipNum, cpVect *resultVerts);

void vertsLocal2World(const cpBody *body, const cpVect *verts, uint num, cpVect *result);

void vertsWorld2Local(const cpBody *body, const cpVect *verts, uint num, cpVect *result);

cpVect vertsNearestEdgeCenter(const cpVect *verts, uint num, cpVect point, cpVect *centerProj);

uint vertsGenerate(float size, uint maxNum, cpVect *result);

void vertsScale(uint num, cpVect *verts, cpVect *result, cpFloat scale);

void vertsPadding(uint num, cpVect *verts, cpVect *result, cpFloat padding);

#endif
