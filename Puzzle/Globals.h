//
//  Globals.h
//  Puzzle
//
//  Created by Bogus on 14.02.2013.
//
//

#ifndef Puzzle_Globals_h
#define Puzzle_Globals_h

#define DEBUG 1
#define STICKY_PADDING 10.0f
#define VERTS_MAX_COUNT 9

#endif
