//
//  Piece.h
//  Puzzle
//
//  Created by Bogus on 24.06.2013.
//
//

#import "CCSprite.h"
#import "chipmunk.h"
#import "MainBoardLayer.h"

@interface Piece : CCSprite <CCTargetedTouchDelegate>
{
    MainBoardLayer *board_;
    cpShape *shape_;
    NSMutableArray *neighbors_;
}

@property (nonatomic, readonly)CGRect rect;
@property (nonatomic, readonly)MainBoardLayer *board;
@property (nonatomic, readonly)cpBody *body;
@property (nonatomic, readonly)cpShape *shape;
@property (nonatomic, retain)NSArray *neighbors;

+ (id)pieceWithTexture:(CCTexture2D *)texture rect:(CGRect)rect inBoard:(MainBoardLayer *)board;
- (id)initWithTexture:(CCTexture2D *)texture rect:(CGRect)rect inBoard:(MainBoardLayer *)board;

@end
