//
//  HelloWorldLayer.h
//  Puzzle
//
//  Created by Bogus on 07.02.2013.
//  Copyright __MyCompanyName__ 2013. All rights reserved.
//


#import <GameKit/GameKit.h>

// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"

// Importing Chipmunk headers
#import "chipmunk.h"

@interface MainBoardLayer : CCLayer <GKAchievementViewControllerDelegate, GKLeaderboardViewControllerDelegate>
{
	CCTexture2D *spriteTexture_; // weak ref
	
	cpSpace *space_; // strong ref
    
    CCScene *scene_; // weak ref
	
	cpShape *walls_[4];
}

@property (nonatomic, readonly)cpSpace *space;

// returns a CCScene that contains the HelloWorldLayer as the only child
+ (CCScene *)scene;

@end
