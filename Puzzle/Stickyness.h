//
//  Stickyness.h
//  Puzzle
//
//  Created by Bogus on 24.06.2013.
//
//

#ifndef Puzzle_Stickyness_h
#define Puzzle_Stickyness_h

#include "chipmunk.h"

enum CollisionType {
    COLLIDE_STICK_SENSOR = 1,
    COLLIDE_STICK_BODY = 2,
};

cpBool stickyBegin(cpArbiter *arb, cpSpace *space, void *data);
cpBool stickyPreSolve(cpArbiter *arb, cpSpace *space, void *data);
void stickyPostSolve(cpArbiter *arb, cpSpace *space, void *data);
void stickySeparate(cpArbiter *arb, cpSpace *space, void *data);

#endif
