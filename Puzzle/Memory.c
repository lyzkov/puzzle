//
//  Memory.c
//  Puzzle
//
//  Created by Bogus on 25.06.2013.
//
//

#include "Memory.h"

void shapeFreeWrap(cpSpace *space, cpShape *shape, void *unused)
{
	cpSpaceRemoveShape(space, shape);
	cpShapeFree(shape);
}

void postShapeFree(cpShape *shape, cpSpace *space)
{
	cpSpaceAddPostStepCallback(space, (cpPostStepFunc)shapeFreeWrap, shape, NULL);
}

void constraintFreeWrap(cpSpace *space, cpConstraint *constraint, void *unused)
{
	cpSpaceRemoveConstraint(space, constraint);
	cpConstraintFree(constraint);
}

void postConstraintFree(cpConstraint *constraint, cpSpace *space)
{
	cpSpaceAddPostStepCallback(space, (cpPostStepFunc)constraintFreeWrap, constraint, NULL);
}

void bodyFreeWrap(cpSpace *space, cpBody *body, void *unused)
{
	cpSpaceRemoveBody(space, body);
	cpBodyFree(body);
}

void postBodyFree(cpBody *body, cpSpace *space)
{
	cpSpaceAddPostStepCallback(space, (cpPostStepFunc)bodyFreeWrap, body, NULL);
}
