//
//  Vertices.c
//  Puzzle
//
//  Created by Bogus on 12.07.2013.
//
//

#include "Vertices.h"
#include "string.h"
#include "gpc.h"


cpVect edgeClipHorizontaly(cpVect A, cpVect B, float x)
{
    cpVect E = cpvsub(B, A);
    
    if (A.x < x && B.x < x) {
        return E.x > 0 ? B : A;
    } else if (A.x > x && B.x > x) {
        return E.x > 0 ? A : B;
    }
    
    float T = (x - A.x) / E.x;
    return cpv(x, A.y + E.y * T);
}

uint vertsClipWithVect(const cpVect *verts, uint num, const cpVect clipVect, cpVect *resultVerts, cpVect *compVerts)
{
    // rotation vector
    cpVect rot = cpvnormalize(clipVect);
    
    // find most extended vertex
    float X = MAXFLOAT;
    for (int i = 0; i < num; i++) {
        cpVect vertex = cpvunrotate(verts[i], rot);
        if (vertex.x < X) {
            X = vertex.x;
        }
    }
    X += cpvlength(clipVect);
    
    
    // clip polygon
    int j = 0;
    int k = 0;
    cpVect S = cpvunrotate(verts[num - 1], rot);
    for (int i = 0; i < num; i++) {
        cpVect vertex = cpvunrotate(verts[i], rot);
        if (vertex.x > X) {
            if (S.x < X) {
                cpVect resVert = cpvrotate(edgeClipHorizontaly(S, vertex, X), rot);
                if (resultVerts) resultVerts[j] = resVert;
                if (compVerts) compVerts[k++] = resVert;
                j++;
            }
            if (resultVerts) resultVerts[j] = cpvrotate(vertex, rot);
            j++;
        } else {
            if (S.x > X) {
                cpVect resVert = cpvrotate(edgeClipHorizontaly(S, vertex, X), rot);
                if (resultVerts) resultVerts[j] = resVert;
                if (compVerts) compVerts[k++] = resVert;
                j++;
            }
            if (compVerts) compVerts[k++] = cpvrotate(vertex, rot);
        }
        S = vertex;
    }
    
    if (j > num || k > num) {
        cpMessage("j > num or k > num", "Stickyness.c", 268, 0, 0, "Warning! Size of one of resulting polygon is larger than input polygon. Possibly array out of bound!!!");
    }
    
    return j;
}

gpc_vertex_list *makeGPCVertexList(const cpVect *verts, uint num)
{
    gpc_vertex_list *vertexList = malloc(sizeof(gpc_vertex_list));
    vertexList->num_vertices = num;
    vertexList->vertex = malloc(num * sizeof(gpc_vertex));
    for (int i = 0; i < num; i++) {
        vertexList->vertex[i].x = (double) verts[i].x;
        vertexList->vertex[i].y = (double) verts[i].y;
    }
    return vertexList;
}

uint vertsIntersectWithVerts(const cpVect *subjectVerts, uint subjectNum, const cpVect *clipVerts, uint clipNum, cpVect *resultVerts)
{
    gpc_polygon subjectPolygon;
    subjectPolygon.num_contours = 1;
    subjectPolygon.hole = malloc(sizeof(int));
    subjectPolygon.hole[0] = 0;
    subjectPolygon.contour = makeGPCVertexList(subjectVerts, subjectNum);
    
    gpc_polygon clipPolygon;
    clipPolygon.num_contours = 1;
    clipPolygon.hole = malloc(sizeof(int));
    clipPolygon.hole[0] = 0;
    clipPolygon.contour = makeGPCVertexList(clipVerts, clipNum);
    
    gpc_polygon resultPolygon;
    gpc_polygon_clip(GPC_INT, &subjectPolygon, &clipPolygon, &resultPolygon);
    
    if (resultPolygon.num_contours == 0) {
        return 0;
    }
    
    gpc_vertex_list *resVList = resultPolygon.contour;
    for (int i = 0; i < resVList->num_vertices; i++) {
        resultVerts[i].x = (CGFloat) resVList->vertex[i].x;
        resultVerts[i].y = (CGFloat) resVList->vertex[i].y;
    }
    
    uint resultNum = resVList->num_vertices;
    
    // free polygons
    gpc_free_polygon(&subjectPolygon);
    gpc_free_polygon(&clipPolygon);
    gpc_free_polygon(&resultPolygon);
    
    return resultNum;
}

void vertsLocal2World(const cpBody *body, const cpVect *verts, uint num, cpVect *result)
{
    for (int i = 0; i < num; i++) {
        result[i] = cpBodyLocal2World(body, verts[i]);
    }
}

void vertsWorld2Local(const cpBody *body, const cpVect *verts, uint num, cpVect *result)
{
    for (int i = 0; i < num; i++) {
        result[i] = cpBodyWorld2Local(body, verts[i]);
    }
}

cpVect vertsNearestEdgeCenter(const cpVect *verts, uint num, cpVect point, cpVect *centerProj)
{
    cpVect result;
    cpVect centerTrans;
    float minDist = MAXFLOAT;
    
    cpVect S = verts[num-1];
    for (int i = 0; i < num; i++) {
        cpVect V = verts[i];
        cpVect E = cpvsub(V, S);
        cpVect P = cpvsub(point, S);
        cpVect proj = cpvadd(S, cpvproject(P, E));
        float dist = cpvdist(point, proj);
        
        if (dist < minDist) {
            minDist = dist;
            centerTrans = cpvmult(E, 0.5);
            result = cpvadd(S, centerTrans);
        }
        S = V;
    }
    
    if (centerProj) {
        *centerProj = result;
    }
    result = cpvadd(result, cpvrperp(centerTrans));
    
    return result;
}

void randVerts(uint *randNums, uint size, uint reps, float mult, cpVect *result)
{
    uint max = sqrtf(size);
    
    for (int i = 0; i < size; i++) {
        randNums[i] = i;
    }
    
    for (int i = 0; i < reps; i++) {
        int idx = random() % (size - i);
        uint val = randNums[idx];
        randNums[idx] = randNums[size-i-1];
        randNums[size-i-1] = val;
        
        float x = (val % max) / (float)max * mult;
        float y = (val / max) / (float)max * mult;
        result[i] = cpv(x, y);
    }
}

uint vertsGenerate(float size, uint maxNum, cpVect *result)
{
    uint reps = 3 + random() % (maxNum-2);
    uint randNums[maxNum];
    
    uint num = 0;
    while (num < 3) {
        randVerts(randNums, maxNum, reps, size, result);
        num = cpConvexHull(reps, result, NULL, NULL, 0.0f);
    }
    
    cpRecenterPoly(num, result);
    
    return num;
}

void vertsScale(uint num, cpVect *verts, cpVect *result, cpFloat scale)
{
	if (result){
		memcpy(result, verts, num*sizeof(cpVect));
	} else {
		result = verts;
	}
    for (int i = 0; i < num; i++) {
        result[i] = cpvmult(result[i], scale);
    }
}

cpVect edgesIntersection(cpVect a, cpVect b, cpVect c, cpVect d)
{
    cpVect deltab = cpvsub(a, b);
    cpVect deltcd = cpvsub(c, d);
    
    cpFloat den = cpvcross(deltab, deltcd);
    cpAssertHard(den != 0, nil);
    
    cpFloat crossAB = cpvcross(a, b);
    cpFloat crossCD = cpvcross(c, d);
    cpFloat x = (crossAB * deltcd.x - crossCD * deltab.x) / den;
    cpFloat y = (crossAB * deltcd.y - crossCD * deltab.y) / den;
    
    return cpv(x, y);
}

void vertsPadding(uint num, cpVect *verts, cpVect *result, cpFloat padding)
{
    cpAssertHard(num > 2, nil);
    
    cpVect X = verts[num-2];
    cpVect S = verts[num-1];
    for (int i = 0; i < num; i++) {
        cpVect V = verts[i];
        cpVect t1 = cpvmult(cpvnormalize(cpvrperp(cpvsub(X, S))), padding);
        cpVect t2 = cpvmult(cpvnormalize(cpvrperp(cpvsub(S, V))), padding);
        
        cpVect a = cpvadd(X, t1);
        cpVect b = cpvadd(S, t1);
        cpVect c = cpvadd(S, t2);
        cpVect d = cpvadd(V, t2);
        result[i] = edgesIntersection(a, b, c, d);
        
        X = S;
        S = V;
    }
}
