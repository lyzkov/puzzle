//
//  Piece.m
//  Puzzle
//
//  Created by Bogus on 24.06.2013.
//
//

#import "Piece.h"
#import "Stickyness.h"
#import "Vertices.h"

#pragma mark - Piece

@interface Piece()

- (cpBody *)createBodyInPhysicsSpace:(cpSpace *)space;
- (BOOL)containsTouchLocation:(UITouch *)touch;

@end

@implementation Piece

enum Layer {
    CollisionLayer = 1,
    SensorLayer = 2
};

@synthesize board = board_;
@synthesize shape = shape_;
@synthesize neighbors = neighbors_;

+ (id)pieceWithTexture:(CCTexture2D *)texture rect:(CGRect)rect inBoard:(MainBoardLayer *)board
{
    return [[[self alloc] initWithTexture:texture rect:rect inBoard:board] autorelease];
}

- (id)initWithTexture:(CCTexture2D *)texture rect:(CGRect)rect inBoard:(MainBoardLayer *)board
{
    self = [super initWithTexture:texture rect:rect];
    if (self) {
        cpSpace *space = board.space;
        board_ = board;
        
        // initialize physics
        cpBody *body = [self createBodyInPhysicsSpace:space];
        
        cpVect verts[VERTS_MAX_COUNT];
        int num = vertsGenerate(200.0f, VERTS_MAX_COUNT, verts);
        
        shape_ = cpPolyShapeNew(body, num, verts, CGPointZero);
        cpShapeSetElasticity(shape_, 0.5f);
        cpShapeSetFriction(shape_, 0.0f);
        cpSpaceAddShape(space, shape_);
        
        cpVect sensorVerts[VERTS_MAX_COUNT];
        vertsPadding(num, verts, sensorVerts, STICKY_PADDING);
        
        cpShape *sensorShape = cpPolyShapeNew(body, num, sensorVerts, cpvzero);
//        cpShapeSetSensor(sensorShape, true);
        cpShapeSetLayers(sensorShape, SensorLayer);
        cpShapeSetCollisionType(sensorShape, COLLIDE_STICK_SENSOR);
        cpShapeSetUserData(sensorShape, shape_);
        cpSpaceAddShape(space, sensorShape);
        
        cpShapeSetCollisionType(shape_, COLLIDE_STICK_BODY);
        cpShapeSetLayers(shape_, CollisionLayer);
        
        cpSpaceAddCollisionHandler(space, COLLIDE_STICK_SENSOR, COLLIDE_STICK_SENSOR, stickyBegin, stickyPreSolve, stickyPostSolve, stickySeparate, NULL);
        //        cpSpaceAddCollisionHandler(space_, COLLIDE_STICK_BODY, COLLIDE_STICK_BODY, NULL, jointPreSolve, NULL, jointSeparate, NULL);
    }
    return self;
}

- (void) dealloc
{
    // shapes and bodies are free in board dealloc
    
	[super dealloc];
}

- (CGRect)rect
{
    CGSize s = self.texture.contentSize;
    return CGRectMake(-s.width / 2, -s.height / 2, s.width / 2, s.height / 2);
}

- (cpBody *)body
{
    return cpShapeGetBody(shape_);
}

#pragma mark - CCNode overridden methods

// returns the transform matrix according the Chipmunk Body values
- (CGAffineTransform)nodeToParentTransform
{
//	CGFloat x = self.body->p.x;
    //	CGFloat y = self.body->p.y;
	CGFloat x = self.position.x;
	CGFloat y = self.position.y;
	
	if ( ignoreAnchorPointForPosition_ ) {
		x += anchorPointInPoints_.x;
		y += anchorPointInPoints_.y;
	}
	
	// Make matrix
	CGFloat c = self.body->rot.x;
	CGFloat s = self.body->rot.y;
	
	if( ! CGPointEqualToPoint(anchorPointInPoints_, CGPointZero) ){
		x += c*-anchorPointInPoints_.x + -s*-anchorPointInPoints_.y;
		y += s*-anchorPointInPoints_.x + c*-anchorPointInPoints_.y;
	}
	
	// Translate, Rot, anchor Matrix
	transform_ = CGAffineTransformMake( c,  s,
									   -s,	c,
									   x,   y );
	
	return transform_;
}

- (void)onEnter
{    
    [[[CCDirector sharedDirector] touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
    
    [super onEnter];
}

- (void)onExit
{
    [[[CCDirector sharedDirector] touchDispatcher] removeDelegate:self];
    [super onExit];
}

#pragma mark - CCSprite overridden methods

// this method will only get called if the sprite is batched.
// return YES if the physic's values (angles, position ) changed.
// If you return NO, then nodeToParentTransform won't be called.
-(BOOL) dirty
{
	return YES;
}

// sync body position with sprite position
- (CGPoint)position
{
    //return cpBodyGetPos(self.body);
    cpBB shapeBB = cpShapeGetBB(shape_);
    float x = shapeBB.l + (shapeBB.r - shapeBB.l)/2;
    float y = shapeBB.b + (shapeBB.t - shapeBB.b)/2;
    return cpv(x, y);
}

// update body when sprite position is changed
- (void)setPosition:(CGPoint)p
{
    [super setPosition:p];
    if (self.body != nil) {
        //        cpBodySetPos(self.body, ccp(p.x, p.y));
        cpBB shapeBB = cpShapeGetBB(shape_);
        float x = shapeBB.l + (shapeBB.r - shapeBB.l)/2;
        float y = shapeBB.b + (shapeBB.t - shapeBB.b)/2;
        cpBodySetPos(self.body, cpvsub(ccp(p.x, p.y), cpBodyWorld2Local(self.body, cpv(x, y))));
        cpSpaceReindexShape(cpBodyGetSpace(self.body), shape_);
    }
}

#pragma mark - CCTargetedTouchDelegate

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
//    touched_ = [self containsTouchLocation:touch];
//    return touched_;
    return [self containsTouchLocation:touch];
}

- (void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
    //    CGPoint oldLocation = [touch previousLocationInView:[touch view]];
    //    oldLocation = [[CCDirector sharedDirector] convertToGL:oldLocation];
    //    oldLocation = [self convertToNodeSpace:oldLocation];
    //    CCLOG(@"old location = %g, %g", oldLocation.x, oldLocation.y);
    //    if (oldLocation.x > 0 && oldLocation.y > 0 && oldLocation.x < self.contentSize.width && oldLocation.y < self.contentSize.height) {
    //        CCLOG(@"new location = %g, %g", [touch locationInView:[touch view]].x, [touch locationInView:[touch view]].y);
    CGPoint location = [self.board convertTouchToNodeSpace:touch];
    [self stopAllActions];
    self.position = ccp(location.x, location.y);
    //    }
}

- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
    self.body->v = cpvzero;
//    touched_ = false;
}

#pragma mark - Private

- (cpBody *)createBodyInPhysicsSpace:(cpSpace *)space
{
    cpBody *body = cpBodyNew(1.0, INFINITY); // no rotation
    
    // TODO: dirty! naughty programmer!
    uint *numCollisions = malloc(sizeof(int));
    *numCollisions = 0;
    cpBodySetUserData(body, numCollisions);
    
    cpSpaceAddBody(space, body);
    return body;
}

- (BOOL)containsTouchLocation:(UITouch *)touch
{
    CGRect bb = self.boundingBox;
    CGPoint location = [touch locationInView:touch.view];
    location = [[CCDirector sharedDirector] convertToGL:location];
    return CGRectContainsPoint(bb, location);
}

@end
